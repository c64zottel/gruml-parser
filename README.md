gruml-parser

This is a small test program. It parses a QML-like file using the *nom* crate
and stores the retrieved data with the help of the *indextree* crate.

The data is intentionally ordered, but the *nom*-part can easily be adapted to allow unordered data.