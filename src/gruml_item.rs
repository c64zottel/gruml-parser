use std::fmt::{Display, Formatter};
use crate::gruml_image::Image;
use crate::gruml_rectangle::Rectangle;
use crate::gruml_text::Text;

#[derive(Debug)]
pub enum GrumlItem {
    Rectangle(Rectangle),
    Text(Text),
    Image(Image),
}

impl Display for GrumlItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(match self {
            GrumlItem::Rectangle(r) => { r.id.as_str() }
            GrumlItem::Text(t) => { t.id.as_str() }
            GrumlItem::Image(i) => { i.id.as_str() }
        })
    }
}