use image::imageops::FilterType;
use indextree::{Arena, NodeId};
use nom::{IResult, bytes::complete::tag, character::complete::multispace0, sequence::tuple, branch::alt};

use nom::multi::many1;
use nom::character::complete::{alphanumeric0, char, u8 as nom_u8};
use nom::combinator::map;
use nom::error::Error;
use nom::number::complete::{float as nom_float};
use nom::sequence::preceded;
use crate::gruml_image::Image;
use crate::gruml_item::GrumlItem;


use crate::gruml_parser::{color_parser, filepath, map_field, text_line_parser, u16_2x, u16_3x};
use crate::gruml_rectangle::Rectangle;
use crate::gruml_text::Text;

pub fn construct_rectangle() -> impl FnMut(&str) -> IResult<&str, (GrumlItem, bool)>
{
    move |input| {
        let mut rectangle = Rectangle::default();
        let (input, complete) = fill_rectangle(&mut rectangle)(input).unwrap();
        Ok((
            input, (GrumlItem::Rectangle(rectangle), complete)
        ))
    }
}

pub fn fill_rectangle<'a>(rec: &mut Rectangle) -> impl FnMut(&str) -> IResult<&str, bool> + '_
{
    move |input| {
        preceded(
            tuple((
                map_field("id", map(alphanumeric0, |v: &str| rec.id = v.to_string())),
                map_field("position", map(u16_3x, |v| rec.position = v)),
                map_field("dimension", map(u16_2x, |v| rec.dimension = v)),
                map_field("color", map(color_parser, |value| rec.color = value)),
                map_field("opacity", map(nom_float, |v: f32| rec.opacity = v)),
                map_field("border.color", map(color_parser, |value| rec.border_color = value)),
                map_field("border.width", map(nom_u8, |v| rec.border_width = v)),
                map_field("radius", map(nom_u8, |v| rec.radius = v)),
            )),
            close_bracket(),
        )(input)
    }
}


pub fn construct_text() -> impl FnMut(&str) -> IResult<&str, (GrumlItem, bool)>
{
    move |input| {
        let mut text = Text::default();
        let (input, complete) = fill_text(&mut text)(input).unwrap();
        Ok((
            input, (GrumlItem::Text(text), complete)
        ))
    }
}


pub fn fill_text(text: &mut Text) -> impl FnMut(&str) -> IResult<&str, bool> + '_
{
    move |input| {
        preceded(
            many1(alt((
                map_field("id", map(alphanumeric0, |v: &str| text.id = v.to_string())),
                map_field("position", map(u16_3x, |v| text.position = v)),
                map_field("text", map(text_line_parser, |v| text.text = v)),
                map_field("color", map(color_parser, |value| text.color = value)),
                map_field("font.pixelSize", map(nom_u8, |v| text.font_pixel_size = v)),
            ))),
            close_bracket(),
        )(input)
    }
}

// pub id: String,
// pub source: String,
// pub position: [u16; 3],
// pub bytes: Vec<u8>,
pub fn construct_image() -> impl FnMut(&str) -> IResult<&str, (GrumlItem, bool)>
{
    move |input| {
        let mut image = Image::default();
        let (input, complete) = fill_image(&mut image)(input).unwrap();
        Ok((
            input, (GrumlItem::Image(image), complete)
        ))
    }
}


pub fn fill_image(image: &mut Image) -> impl FnMut(&str) -> IResult<&str, bool> + '_
{
    move |input| {
        let result = preceded(
            tuple((
                map_field("id", map(alphanumeric0, |v: &str| image.id = v.to_string())),
                map_field("position", map(u16_3x, |v| image.position = v)),
                map_field("dimension", map(u16_2x, |v| image.dimension = v)),
                map_field("source", map(filepath, |v| { image.source = v })), // load and map bytes
            )),
            close_bracket(),
        )(input);

        let tmp_image = image::open(&image.source).expect(&*format!("File: {}", image.source));
        let tmp_image = tmp_image.resize(image.dimension[0] as u32,
                                         image.dimension[1] as u32,
                                         FilterType::Triangle);

        image.dimension = [tmp_image.width() as u16, tmp_image.height() as u16];
        image.image = tmp_image;

        result
    }
}

pub fn close_bracket() -> impl FnMut(&str) -> IResult<&str, bool>
{
    move |input| {
        match tuple((multispace0::<&str, Error<&str>>, tag("}"), multispace0))(input) {
            Ok((remainder, _)) => Ok((remainder, true)),
            Err(_) => Ok((input, false)),
        }
    }
}


pub fn gruml_parser(mut input: &str) -> IResult<&str, Arena<GrumlItem>> {
    let mut arena = Arena::new();
    let mut none_complete_ids = Vec::<NodeId>::new();

    'outer: loop {
        let gruml_item;
        let mut complete;
        (input, (gruml_item, complete)) = alt((
            preceded(
                tuple((multispace0, tag("Rectangle"), multispace0, char('{'), multispace0)),
                construct_rectangle(),
            ),
            preceded(
                tuple((multispace0, tag("Text"), multispace0, char('{'), multispace0)),
                construct_text(),
            ),
            preceded(
                tuple((multispace0, tag("Image"), multispace0, char('{'), multispace0)),
                construct_image(),
            )
        ))(input).unwrap();
        let new_node = arena.new_node(gruml_item);

        none_complete_ids.last().map(|parent_node_id| parent_node_id.append(new_node, &mut arena));


        // complete == true, we expect:
        // - a closing bracket
        // - a sibling
        // - EOF
        // complete == false, we expect:
        // - children
        loop {
            if complete {
                (input, complete) = close_bracket()(input).unwrap();

                if complete {
                    none_complete_ids.pop();
                    if none_complete_ids.is_empty() {
                        break 'outer;
                    }
                    continue;
                } else {
                    // Necessary for single items with only one block
                    if input.is_empty() && none_complete_ids.is_empty() {
                        break 'outer;
                    }
                }

                break;
            } else {
                none_complete_ids.push(new_node);
                break;
            }
        }
    }

    Ok((input, arena))
}

