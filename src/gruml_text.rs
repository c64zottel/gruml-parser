use std::fmt::{Debug, Formatter};
use crate::gruml_color::Color;

#[derive(Default, Clone)]
pub struct Text {
    pub id: String,
    pub text: String,
    pub position: [u16; 3],
    pub color: Color,
    pub font_pixel_size: u8,
}

impl Debug for Text {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let indent = f.width().unwrap_or_default();
        let mut s = String::default();

        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("id: {:?}", self.id);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("text: {:?}", self.text);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("position: {:?}", self.position);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("color: {:?}", self.color);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("font size: {:?}", self.font_pixel_size);

        s += &format!("\n");

        f.write_str(s.as_str())
    }
}