use std::fmt::{Debug, Display, Formatter};
use crate::gruml_color::Color;

//TODO
// everything u16 (u8 wtf?)
// id to unique hash
// getter + setter -> trait in the future?

#[derive(Default, Clone)]
pub struct Rectangle {
    pub radius: u8,
    pub border_width: u8,
    pub position: [u16; 3],
    pub dimension: [u16; 2],
    pub(crate) border_color: Color,
    pub(crate) color: Color,
    pub opacity: f32,
    pub id: String,
    pub children: Vec<Rectangle>,
}

impl Rectangle {
    pub fn dimension(&self) -> [u16; 2] { self.dimension }
    pub fn position(&self) -> [u16; 3] { self.position }
    pub fn radius(&self) -> u8 { self.radius }
    pub fn color(&self) -> &[u8; 3] { self.color.value() }
    pub fn id(&self) -> String { self.id.clone() }
}


impl Debug for Rectangle {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let indent = f.width().unwrap_or_default();
        let mut s = String::default();

        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("id: {:?}", self.id);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("dimension: {:?}", self.dimension);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("position: {:?}", self.position);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("color: {:?}    opacity: {:?}", self.color, self.opacity);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("radius: {:?}    border_width: {:?}    border_color: {:?}", self.radius, self.border_width, self.border_color);

        s += &format!("\n");

        for x in &self.children {
            s += &format!("{:>ident$?}", x, ident = (4 + indent));
        };

        f.write_str(s.as_str())
    }
}

impl Display for Rectangle {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let indent = f.width().unwrap_or_default();
        let mut s = String::default();

        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("id: {:?}", self.id);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("dimension: {:?}", self.dimension);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("position: {:?}", self.position);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("color: {:?}    opacity: {:?}", self.color, self.opacity);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("radius: {:?}    border_width: {:?}    border_color: {:?}", self.radius, self.border_width, self.border_color);

        s += &format!("\n");

        for x in &self.children {
            s += &format!("{:>ident$?}", x, ident = (4 + indent));
        };

        f.write_str(s.as_str())
    }
}