use gruml::gruml_item::GrumlItem;
use gruml::gruml_parser::file_to_objects;


// WS = 1*(WSP / CRLF)
// NUM = *DIGIT
// Rectangle = WS 'Rectangle' WS '{' WS *item WS '}'
// item = property_name ':' (*NUM | exp) ( ';' item / [';'] CRLF )
// item =/ property_color ':' color ( ';' item / [';'] CRLF )
// property_name = 'width' / 'height' / 'radius' / 'border.width'
// property_color = 'color' / 'border.color'
// color = svg_color / color description / expr?
// type Res<T, U> = IResult<T, U, VerboseError<T>>;


fn main() {
    let (remaining, mut arena) = file_to_objects("src/samples/rectangle.gruml");

    println!(" -- remainder:\n{}\n -- remainder end\n", remaining);

    let x = arena.iter_mut().next().unwrap().get_mut();
    match x {
        GrumlItem::Rectangle(r) => {r.radius = 37}
        GrumlItem::Text(_) => {}
        GrumlItem::Image(_) => {}
    }

    let x = arena.iter().next().unwrap();
    let n1 = arena.get_node_id(x).unwrap();
    println!("{:?}", n1.debug_pretty_print(&arena));
}
