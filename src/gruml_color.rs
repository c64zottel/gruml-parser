#[derive(Default, Debug, Copy, Clone)]
pub struct Color {
    value: [u8; 3],
}

impl Color {
    pub fn new(color: [u8; 3]) -> Self {
        Self {
            value: color,
        }
    }

    pub fn value(&self) -> &[u8; 3] {
        &self.value
    }
}