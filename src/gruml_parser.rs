use std::fs::read_to_string;
use indextree::Arena;
use nom::{IResult, bytes::complete::tag, character::complete::{char, multispace0}, combinator::map, sequence::tuple, branch::alt, error::Error, InputLength, InputTake, Compare, AsChar};
use nom::bytes::complete::is_not;
use nom::character::complete::{alphanumeric1, multispace1, space1};
use nom::multi::{fold_many1, many1, separated_list1};
use nom::character::complete::u16 as nom_16;
use nom::error::ErrorKind;
use nom::sequence::delimited;
use crate::constructors::gruml_parser;
use crate::gruml_color::Color;
use crate::gruml_item::GrumlItem;

pub fn map_field<'a, K, P, O>(key: K, parser: P) -> impl FnMut(&'a str) -> IResult<&'a str, O>
    where
        P: nom::Parser<&'a str, O, Error<&'a str>>,
        K: InputLength + Clone,
        &'a str: Compare<K>
{
    delimited(
        tuple((tag(key), multispace0, char(':'), multispace0)),
        parser,
        remove_space_delimiter_space(";"),
    )
}


// at least 1 multispace or 1 key must be present
// TODO: allow key only once
fn remove_space_delimiter_space<T>(key: T) -> impl FnMut(T) -> IResult<T, ()>
    where
        T: InputTake + Compare<T> + Clone + nom::InputTakeAtPosition + InputLength,
        <T as nom::InputTakeAtPosition>::Item: AsChar + Clone,
{
    map(
        many1(alt((
            multispace1,
            tag(key),
        ))),
        |_| (),
    )
}

pub fn u16_2x(input: &str) -> IResult<&str, [u16; 2]>
{
    let result = separated_list1(
        multispace1,
        nom_16,
    )(input);

    match result {
        Ok((rem, v)) => {
            let f = match v.len() {
                2 => [v[0], v[1]],
                _ => return Err(nom::Err::Error(Error::new("Wrong number of integers.", ErrorKind::LengthValue))),
            };
            Ok((rem, f))
        }
        Err(e) => { Err(e) }
    }
}

pub fn u16_3x(input: &str) -> IResult<&str, [u16; 3]>
{
    let result = separated_list1(
        multispace1,
        nom_16,
    )(input);

    match result {
        Ok((rem, v)) => {
            let f = match v.len() {
                2 => [v[0], v[1], 0],
                3 => [v[0], v[1], v[2]],
                _ => return Err(nom::Err::Error(Error::new("Wrong number of integers.", ErrorKind::LengthValue))),
            };

            Ok((rem, f))
        }
        Err(e) => { Err(e) }
    }
}

pub fn text_line_parser(input: &str) -> IResult<&str, String>
{
    let result: IResult<&str, String, Error<&str>> =
        delimited(char('"'),
                  fold_many1(alt((alphanumeric1, space1, is_not("\""))),
                             String::new,
                             |mut acc: String, s: &str| {
                                 acc.extend(s.chars());
                                 acc
                             }),
                  char('"'))
            (input);

    match result {
        Ok((rem, v)) => { Ok((rem, v)) }
        Err(e) => { Err(e) }
    }
}

pub fn filepath(input: &str) -> IResult<&str, String>
{
    let result: IResult<&str, String, Error<&str>> =
        fold_many1(
            alt((
                alphanumeric1,
                tag("/"),
                tag("_"),
                tag("."),
            )),
            String::new,
            |mut acc: String, s: &str| {
                acc.extend(s.chars());
                acc
            })
            (input);

    match result {
        Ok((rem, v)) => { Ok((rem, v)) }
        Err(e) => { Err(e) }
    }
}

pub fn color_parser(input: &str) -> IResult<&str, Color>
{
    let result = alt((
        alphanumeric1,
        delimited(char('"'), alphanumeric1, char('"')),
    ))(input);

    match result {
        Ok((rem, v)) => { Ok((rem, Color::new(palette::named::from_str(v).unwrap().into()))) }
        Err(e) => { Err(e) }
    }
}

pub fn file_to_objects<T: AsRef<std::path::Path>>(file_name: T) -> (String, Arena<GrumlItem>)
{
    let qml_text = read_to_string(file_name).unwrap();
    let (remaining, objects) = gruml_parser(qml_text.as_str()).unwrap();

    (remaining.clone().to_string(), objects)
}