use std::fmt::{Debug, Formatter};
use image::DynamicImage;

#[derive(Default, Clone)]
pub struct Image {
    pub id: String,
    pub position: [u16; 3],
    pub dimension: [u16; 2],
    pub source: String,
    pub image: DynamicImage,
}

impl Debug for Image {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let indent = f.width().unwrap_or_default();
        let mut s = String::default();

        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("id: {:?}", self.id);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("position: {:?}", self.position);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("dimension: {:?}", self.dimension);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("source: {:?}", self.source);
        s += &format!("\n{:indent$}", "", indent = indent);
        s += &format!("number of bytes: {:?} (MiB: {:.2})", self.image.as_bytes().len(), self.image.as_bytes().len() as f32 / (1024.0 * 1024.0));

        s += &format!("\n");

        f.write_str(s.as_str())
    }
}